using System.Collections;
using System.Collections.Generic;
using System.IO.Pipes;
using System.Runtime.CompilerServices;
using Unity.VisualScripting;
using UnityEngine;

public class PoleSpawnScript : MonoBehaviour
{
    public GameObject poles;
    public float spawnRate = 3;
    public float spawnOffsetY = 20;
    private float timeSinceSpawn = 0;

    void Start()
    {
        spawnPoles();
    }

    void Update()
    {
        timeSinceSpawn += Time.deltaTime;

        if (timeSinceSpawn > spawnRate)
        {
            spawnPoles();
            timeSinceSpawn = 0;
        } 
    }

    private void spawnPoles()
    {
        // Spawn poles at the position and rotation of the spawner itself
        float randomY = Random.value * spawnOffsetY;
        if (randomY % 2 == 0)
        {
            randomY *= -1;
        }

        Vector3 position = new Vector3(transform.position.x, randomY, 0);

        Instantiate(poles, position, transform.rotation);
    }
}
