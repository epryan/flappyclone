using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameLogicScript : MonoBehaviour
{
    public GameObject knockout;
    public GameObject titleScreen;
    private bool gamePaused = false;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (gamePaused)
            {
                UnPauseGame();
                gamePaused = false;
            } else
            {
                PauseGame();
                gamePaused = true;
            }
        }

    }

    private void Start()
    {
        PauseGame();
    }

    public void StartGame()
    {
        titleScreen.SetActive(false);
        UnPauseGame();
    }

    public void PauseGame()
    {
        Time.timeScale = 0;
    }

    public void UnPauseGame()
    {
        Time.timeScale = 1;
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        knockout.SetActive(false);
    }

    public void GameOver()
    {
        knockout.SetActive(true);
    }

}
