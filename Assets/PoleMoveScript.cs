using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoleMoveScript : MonoBehaviour
{
    public float moveSpeed;
    private GameObject LevelBoundaryLeft;

    void Start()
    {
        LevelBoundaryLeft = GameObject.Find("LevelBoundaryLeft");
    }

    void Update()
    {
        if (transform.position.x < LevelBoundaryLeft.transform.position.x)
        {
            Debug.Log("Destroying poles.");
            Destroy(gameObject);
        }
        transform.position = transform.position + Vector3.left * moveSpeed * Time.deltaTime;
    }

}
