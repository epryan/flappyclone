using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;
using UnityEngine.UI;
using TMPro;

public class FlapScript : MonoBehaviour {
    public Rigidbody2D flapBody;
    private bool flapEnabled = true;
    public float flapVelocity;
    private int score = 0;
    public TMP_Text tmpTextScore;
    public GameLogicScript gameLogic;
    private GameObject LevelBoundaryTop;
    private GameObject LevelBoundaryBottom;


    void Start()
    {
        LevelBoundaryTop = GameObject.Find("LevelBoundaryTop");
        LevelBoundaryBottom = GameObject.Find("LevelBoundaryBottom");
    }

    void Update()
    {
        float y = transform.position.y;
        if (y < LevelBoundaryBottom.transform.position.y || y > LevelBoundaryTop.transform.position.y)
        {
            playerDeath();
        }

        if (flapEnabled && (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Mouse0)))
        {
        flapBody.velocity = Vector2.up * flapVelocity;
        }
    }

    private void playerDeath()
    {
        flapEnabled = false;
        flapBody.velocity = Vector2.down * 25 + Vector2.left * 25;
        gameLogic.GameOver();
        Debug.Log("Died with " + score + " points.");
    }

    private void OnCollisionEnter2D()
    {
        playerDeath();
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.name == "Poles(Clone)")
        {
            score += 1;
            tmpTextScore.text = score.ToString();
        }
    }
}
